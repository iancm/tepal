/*
 * genetics.h
 *
 * This file implements a struct, genome, that represents the genetic code of
 * an organism. It includes functions to simulate genetic reproduction and to
 * decode the genetic information.
 *
 * STRUCTURE OF THE GENOME
 * The genome contains a pair of chromosome arrays; each chromosome is paired
 * with another from the other array, one-to-one. In haploid genomes, the
 * right chromosomes are not present.
 * A chromosome is an array of genes.
 * Each chromosome is considered as a unit during sexual reproduction.
 * Finally, an array of chromosome flags is included, corresponding to each
 * respective chromosome pair.
 */

#include "pcgrand.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef uint8_t Gene;
typedef uint8_t* Chromosome;

/*
 * Chromosome flags. Used as bit field according to defines below
 * Reminder: when adding a new flag, update copy_chflag()!
 */
typedef uint8_t Chflag;
#define ALLOSOME 1u	/* This chromosome is an allosome if set */

typedef struct genome
{
	int len;			/* Length of chromosome in Genes */
	int ploidy;			/* Number of UNIQUE chromosomes */
	Chflag* flags;		/* Flags for chromosomes */
	Chromosome* left;		/* Array of unique chromosomes */
	Chromosome* right;		/* Array of pairs to the left chromosomes */
} Genome;

//extern uint64_t* gen_rand; /* Random number generator state; see pcgrand.h */
uint64_t* gen_rand;

/*
 * Relating to the construction and destruction of genomes
 */
// Creates empty genome with (to_alloc) chromosome arrays allocated
Genome* create_genome(int len, int ploidy, int to_alloc);
/* Copy genome "src" and return copy
 * copy_chromosome is a helper
 */
Genome* copy_genome(const Genome* src);
void copy_chromosome(Chromosome dest, const Chromosome src, int len);
// Randomize genome data, keeping len and ploidy the same.
int randomize_genome(Genome*);
// Frees memory of genome 
void delete_genome(Genome*);

/*
 * Relating to sexual reproduction
 */
// Creates haploid genome from a parent genome, which may be either diploid or haploid.
Genome* make_haploid(const Genome* src);
// Executes sexual reproduction between two genomes, resulting in a diploid child.
// Parent genomes may be either diploid or haploid; both are accounted for.
Genome* sexrep(const Genome* x, const Genome* y);
// Determines chromosome flags in child
Chflag copy_chflag(Chflag x, Chflag y);
// Deletes temporary haploid gen ome used in sexrep
void delete_temp_haploid(Genome*);
// Executes chromosomal crossover across genome
void crossover(Genome* g);


/*
 * Relating to the decoding of genomes
 */
// Prints contents of genome
void print_genome(const Genome*);
/*
 * Loads the Gene found at chromosome[index] in g into "left" and "right"
 * Returns zero on failure, nonzero on success
 * May fail if e.g. chromosome > ploidy or index > len
 */
bool get_gene(const Genome* g, Gene* left, Gene* right,
		int chromosome, int index);
/*
 * Tests whether the Gene found at chromosome, index in g equals target
 * Returns  3 	 if both left and right match,
 * 	"		2 	 if only left matches
 * 	"		1 	 if only right matches
 * 	"		0 	 if neither match
 */
int equals_gene(const Genome* g, Gene target,
		int chromosome, int index);
/*
 * Tests whether a subset of the bits in the Gene found at chromosome, index in gequals target
 * using a mask; target includes values to test against and mask indicates bits to test
 * e.g. if	target	=	01001101
 * 		and	mask	=	11000011
 * 		and gene 	=	01110001
 * 		then consider gene == target to be true
 * Return values are identical to return values of compare_gene(...)
 */
int compare_gene(const Genome* g, Gene target, Gene mask,
		int chromosome, int index);
/*
 * Returns a Chflag at given index
 */
Chflag get_chflag(const Genome* g, int index);
/*
 * Tests whether a Chflag at given index equals target
 * Returns 	1 	if true
 * 	"		0	if false
 */
bool equals_chflag(const Genome* g, Chflag target, int index);
/*
 * Returns a difference between target at the Chflag at given index via xor
 * Returns 0 at each bit that is different; 0 at each bit that is the same
 * Therefore, returns 0 if Chflags are identical.
 */
Chflag compare_chflag(const Genome* g, Chflag target, int index);

/*
 * Helper functions
 * The following functions are primarily helper functions within genetics;
 * use outside of genetics.c is probably unnecessary and a symptom of
 * poor design decisions.
 */
