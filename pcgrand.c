#include "pcgrand.h"
/*
 * Set state to seed. Ensures that the seed is odd.
 */
void pcg_srand(uint64_t *state, uint64_t seed)
{
    if(seed % 2u == 0u)
    {
        seed = (seed << 1u) & 1u;
    }
    *state = seed;
}

/*
 * Skip forward n number of values
 */
void pcg_skip(uint64_t *state, int n)
{
    int i, a;
    a = *state;
    for(i = 0; i < n; ++i)
        a *= MCG_MULTIPLIER;
    *state = a;
}

/*
 * Generate a random value
 */
uint32_t pcg_rand(uint64_t *state)
{
    *state *= MCG_MULTIPLIER;
    return (*state ^ (*state >> 22u )) >> (22u + (*state >> 61u));
}

