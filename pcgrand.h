/*
 * pcgrand.h
 * This file implements a random number generator suitable for the generation
 * of unsigned 32-bit integers.
 *
 * The specific RNG uses 64 bits of state that are cycled using a
 * multiplicative congruential generator, which is used to create a 32-bit
 * output based on the PCG-XSH-RS generator detailed in the paper
 * "PCG: A Family of Simple Fast Space-Efficient Statistically Good
 *  Algorithms for Random Number Generation" by Melissa E. O'Neill.
 * The multiplier used for the MCG is chosen because it has an acceptable
 * multiplicative order modulo 2^32 and 2^64.
 *
 * This RNG has been tested with TestU01 on default settings. It passes
 * BigCrush with no failures.
 */

#ifndef PCGRAND_H
#define PCGRAND_H

/*
 * 8^20 + 3
 * Value chosen after testing with TestU01
 */
#define MCG_MULTIPLIER 1152921504606846979ul;

#include <stdint.h>

/*
 * Set the state to a certain seed. This sets state to the seed if the
 * seed is odd; otherwise, it makes the seed odd and sets the state to that.
 * It is important that the seed is odd in order to preserve cryptographic
 * strength of the MCG. Probably.
 */
void pcg_srand(uint64_t *state, uint64_t seed);

/*
 * Skips the next n values. Equivalent to running pcg_rand() n times.
 */
void pcg_skip(uint64_t *state, int n);

/*
 * Gets the next random number in the sequence, and updates the state.
 */
uint32_t pcg_rand(uint64_t *state);

#endif
