CC=gcc
CFLAGS=-g -Wall -Wextra -Werror -pedantic -Wpadded
CFLAGSO=-Wall -Wextra -pedantic -O3 -flto #Optimized CFLAGS
STD=-std=c99
HEADERS=genetics.h pcgrand.h
OBJECTS=genetics.o pcgrand.o

%.o : %.c $(HEADERS)
	$(CC) -c -o $@ $< $(CFLAGS) $(STD)

tepal: $(OBJECTS)
	$(CC) -o $@ $^ $(CFLAGS) $(STD)

clean:
	rm tepal *.o
