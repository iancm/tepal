/* genetics.c */

#include "genetics.h"

/* Creates an empty genome. to_alloc indicates how many chromosome arrays to allocate:
 * 		0	allocates no chromosome arrays
 * 		1	allocates just the left chromosome array; haploid
 * 		2	allocates both left and right chromosome arrays; diploid
 */
Genome* create_genome(int len, int ploidy, int to_alloc)
{
	if(len <= 0 || ploidy <= 0)
		return NULL;
	Genome* new_genome = malloc(sizeof(Genome));
	new_genome->len = len;
	new_genome->ploidy = ploidy;
	
	// Create Chromosome arrays
	if(to_alloc > 0)
		new_genome->left = malloc(sizeof(Chromosome) * ploidy);
	else
		new_genome->left = NULL;
	if(to_alloc == 2)
		new_genome->right = malloc(sizeof(Chromosome) * ploidy);
	else
		new_genome->right = NULL;
	/* Create arrays of (len) Genes within each Chromosome */
	if(to_alloc > 0)
		for(int i = 0; i < ploidy; ++i)
		{
			new_genome->left[i] = calloc(len, sizeof(Gene));
			if(to_alloc == 2)
				new_genome->right[i] = calloc(len, sizeof(Gene));
		}
	/* Create chromosome flags */
	new_genome->flags = calloc(ploidy, sizeof(Chflag));
	return new_genome;
}

/*
 * Makes a copy of src and returns a pointer to the copy.
 * Accepts genomes whether diploid or haploid. Does not accept aploid sources
 */
Genome* copy_genome(const Genome* src)
{
	if(src == NULL)
		return NULL;
	assert(src->left != NULL);
	const int len = src->len;
	const int ploidy = src->ploidy;
	const int haploid = (src->right == NULL);
	Genome* copy = create_genome(len, ploidy, (haploid) ? 1 : 2);
	copy->len = len;
	copy->ploidy = ploidy;

	for(int i = 0; i < ploidy; ++i)
	{
		copy_chromosome(copy->left[i], src->left[i], len);
		if(!haploid)
			copy_chromosome(copy->right[i], src->right[i], len);
		copy->flags[i] = src->flags[i];
	}
	return copy;
}

void copy_chromosome(Chromosome dest, const Chromosome src, int len)
{
	assert(src != NULL && len > 0);
	for(int i = 0; i < len; ++i)
		dest[i] = src[i];
	return;
}

/*
 * Fills chromosomes with random data
 * Accepts haploid and diploid genomes only
 */
int randomize_genome(Genome* g)
{
	if(g == NULL)
		return 0;
	assert(g->left != NULL);
	const int len = g->len;
	const int ploidy = g->ploidy;
	const int haploid = (g->right == NULL);
	int i,j;
	for(i = 0; i < ploidy; ++i)
	{
		for(j = 0; j < len; ++j)
		{
			uint32_t rand = pcg_rand(gen_rand);
			g->left[i][j] = rand & 0xFFu;
			if(!haploid)
				g->right[i][j] = (rand >> 8) & 0xFFu;
		}	
	}
	return i*j;	/* Number of genes randomized */
}

void delete_genome(Genome* g)
{
	if(g == NULL)
		return;
	int haploid = (g->right == NULL);
	for(int i = 0; i < g->ploidy; ++i)
	{
		free(g->left[i]);
		if(!haploid)
			free(g->right[i]);
	}
	free(g->left);
	free(g->right);
	free(g->flags);
	free(g);
	g = NULL;
}

/*
 * make_haploid chooses a random chromosome from each pair and copies it into the child's left.
 * make_haploid accepts either diploid or haploid sources.
 * If source is diploid, executes chromosomal crossover.
 */
Genome* make_haploid(const Genome* src)
{
	if(src == NULL)
		return NULL;
	const int len = src->len;
	const int ploidy = src->ploidy;
	Genome* child = copy_genome(src);
	if(src->right == NULL)
	{
		// src is already haploid! Copy it and return
		return child;
	}
	crossover(child);

	/*
	 * Choose random source chromosomes to keep.
	 * This is done by randomly copying entire right chromosomes into their left pairs,
	 * then keeping only the left chromosome.
	 */
	// rand and j are used to determine a random bit
	uint32_t rand = pcg_rand(gen_rand);
	for(int i = 0, j = 0; i < ploidy; ++i, ++j)
	{
		// uses each bit of rand in sequence, marching through using j
		// it may be simpler and faster to just keep generating new random numbers;
		// I haven't really thought that through. This should be fast though, and hey! it works.
		if(j >= 32)
			j = 0;
		// (rand & (1u << j)) results in 0 or !0 equally often.
		// If !0, copy the right chromosome into the left chromosome.
		if(rand & (1u << j))
			copy_chromosome(child->left[i], child->right[i], len);
		free(child->right[i]);
	}
	free(child->right);
	child->right = 0;
	return child;
}

/*
 * Simulates sexual reproduction between parent genomes x,y producing child genome,
 * which is returned. Each parent may be either diploid or haploid, and need not match.
 */
Genome* sexrep(const Genome* x, const Genome* y)
{
	// Initialize child genome
	const int len = x->len;
	const int ploidy = x->ploidy;
	assert(y->len == len && y->ploidy == ploidy);
	Genome* child = create_genome(len, ploidy, 0);

	// Create diploid genomes from parents
	Genome* x_haploid = make_haploid(x);
	Genome* y_haploid = make_haploid(y);

	// Copy diploid genomes into child
	child->left = x_haploid->left;
	child->right = y_haploid->left;
	for(int i = 0; i < ploidy; ++i)
		child->flags[i] = copy_chflag(x->flags[i], y->flags[i]);

	// Parent diploids are now useless
	delete_temp_haploid(x_haploid);
	delete_temp_haploid(y_haploid);

	return child;
}

/*
 * Determines chromosome flags in a child.
 * Each chromosome flag is unique, and should be handled seperately.
 * Make sure to update when adding a new chromosome flag!
 */
Chflag copy_chflag(Chflag x, Chflag y)
{
	Chflag c = 0u;
	
	// Allosomes always continue to be allosomes.
	// Assert equal because whether a chromosome is an allosome will never change
	// and should be constant across the species.
	assert((x & ALLOSOME) == (y & ALLOSOME));
	c &= x & ALLOSOME;

	return c;
}

// Delete as normal, but leave left chromosome, which is still in use by child.
void delete_temp_haploid(Genome* g)
{
	if(g == NULL)
		return;
	assert(g->right == NULL);	// As g is haploid
	free(g->flags);
	free(g);
}

/*
 * Executes chromosomal crossover.
 * Chooses random contiguous section of each chromosome to trade with its pair.
 * e.g. Given a chromosome pair:
 * 		left:	AAAAAAAAAAAAAAA
 * 		right:	BBBBBBBBBBBBBBB
 * this function may output:
 * 		left:	AAAAABBBBAAAAAA
 * 		right:	BBBBBAAAABBBBBB
 * If the ALLOSOME flag is set for this chromosme, chromosomal crossover is not executed.
 * Due to the way start and end are determined, there is a ~50% chance that
 * crossover is not executed for any particular chromosome. This is intentional behavior.
 */
void crossover(Genome* g)
{
	const int len = g->len;
	for(int i = 0; i < g->ploidy; ++i)
	{
		/* Algorithm:
		 * - generate random start and end indices between 0 and len
		 * - if start <= end, switch Genes with indices on the interval [start,end]
		 * - if start > end, don't do anything
		 */
		if(!(g->flags[i] & ALLOSOME))
		{
			uint32_t rand = pcg_rand(gen_rand);
			int start = rand % len;
			int end = (rand >> 16) % len;
			for(int j = start; j <= end; ++j)
			{
				Gene temp = g->left[i][j];
				g->left[i][j] = g->right[i][j];
				g->right[i][j] = temp;
			}
		}
	}
}

/*
 * Prints the Genes in the genome. For debug and testing; user should not see this.
 */
void print_genome(const Genome* g)
{
	if(g == NULL)
	{
		printf("Empty genome encountered.\n");
		return;
	}
	const int len = g->len;
	const int ploidy = g->ploidy;
	const int haploid = (g->right == NULL);
	printf("len: %d\tploidy: %d", len, ploidy);
	for(int i = 0; i < ploidy; ++i)
	{
		printf("\n%d is %ssome\n", i, (g->flags[i] & ALLOSOME) ? "allo" : "auto");
		printf("\n%dL:\t", i);
		for(int j = 0; j < len; ++j)
			printf("%u\t", g->left[i][j]);
		if(!haploid)
		{
			printf("\n%dR:\t", i);
			for(int j=0; j < len; ++j)
				printf("%u\t", g->right[i][j]);
		}
	}
	printf("\n");
}

/*
 * Loads the Gene found at chromosome[index] into left and right arguments
 * returns false if failure, else true
 * Use this function if you want a copy of a Gene in the Genome g
 */
bool get_gene(const Genome* g, Gene* left, Gene* right,
		int chromosome, int index)
{
	assert(g != NULL && left != NULL && right != NULL);
	if(chromosome >= g->ploidy || index >= g->len)
		return false;

	*left = g->left[chromosome][index];
	*right = g->right[chromosome][index];
	
	return true;
}

/*
 * Tests whether the Gene found at chromosome, index in g equals target
 * Returns  3 	 if both left and right match,
 * 	"		2 	 if only left matches
 * 	"		1 	 if only right matches
 * 	"		0 	 if neither match
 */
int equals_gene(const Genome* g, Gene target,
		int chromosome, int index)
{
	assert(g != NULL && chromosome < g->ploidy && index < g->len);

	int temp = 0;
	if(target == g->left[chromosome][index])
		temp &= 2;
	if(target == g->right[chromosome][index])
		temp &= 1;
	return temp;
}

/*
 * Tests whether a subset of the bits in the Gene found at chromosome, index in gequals target
 * using a mask; target includes values to test against and mask indicates bits to test
 * e.g. if	target	=	01001101
 * 		and	mask	=	11000011
 * 		and gene 	=	01110001
 * 		return true
 * Return values are identical to return values of compare_gene(...)
 */
int compare_gene(const Genome* g, Gene target, Gene mask,
		int chromosome, int index)
{
	assert(g != NULL && chromosome < g->ploidy && index < g->len);

	int temp = 0;
	if((target & mask) == (g->left[chromosome][index] & mask))
		temp &= 2;
	if((target & mask) == (g->right[chromosome][index] & mask))
		temp &= 1;
	return temp;
}

/*
 * Returns a Chflag at given index
 */
Chflag get_chflag(const Genome* g, int index)
{
	assert(g != NULL && index < g->ploidy);
	return g->flags[index];
}

/*
 * Tests whether a Chflag at given index equals target
 */
bool equals_chflag(const Genome* g, Chflag target, int index)
{
	assert(g != NULL && index < g->ploidy);
	return (target == g->flags[index]);
}

/*
 * Returns a difference between target at the Chflag at given index via xor
 * Returns 0 at each bit that is different; 0 at each bit that is the same
 * Therefore, returns 0 if Chflags are identical.
 */
Chflag compare_chflag(const Genome* g, Chflag target, int index)
{
	assert(g != NULL && index < g->ploidy);
	return target ^ g->flags[index];
}

/* Test function */
int main(void)
{
	gen_rand = malloc(sizeof(uint64_t));
	*gen_rand = 56465415364421u;

	// Test creation and copy
	Genome* original, *copy;
	original = create_genome(8, 4, 2);
	if(randomize_genome(original))
		copy = copy_genome(original);
	printf("TRIVIAL TEST\noriginal:\n");
	print_genome(original);
	printf("copy:\n");
	print_genome(copy);
	delete_genome(original);
	delete_genome(copy);

	// Test sexual reproduction
	Genome* mama, *papa, *child;
	mama = create_genome(8, 4, 2);
	randomize_genome(mama);
	papa = create_genome(8, 4, 2);
	randomize_genome(papa);
	child = sexrep(mama, papa);
	printf("SEXREP TEST\nmama:\n");
	print_genome(mama);
	printf("papa:\n");
	print_genome(papa);
	printf("child:\n");
	print_genome(child);
	delete_genome(mama);
	delete_genome(papa);
	delete_genome(child);


	free(gen_rand);
	return 0;
}
